#!/usr/bin/env python
# coding: utf-8

import random
import numpy as np
import time
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import math
import pandas as pd
import random
import matplotlib.pyplot as plt


#importing data
file = pd.read_excel("CCPP/CCPP/Folds5x2_pp.xlsx")
data = pd.DataFrame(file)

#spliting data into train and test with 80% training data and 20% testing data
train, test = train_test_split(data, test_size=0.2, shuffle=True)

#Training data
l1_train = list(train['AT'])
l2_train = list(train['V'])
l3_train = list(train['AP'])
l4_train = list(train['RH'])
l5_train = list(train['PE'])

#normalising training data
new_max = 1
new_min = 0
max_in1 = max(l1_train)
max_in2 = max(l2_train)
max_in3 = max(l3_train)
max_in4 = max(l4_train)
max_in5 = max(l5_train)
min_in1 = min(l1_train)
min_in2 = min(l2_train)
min_in3 = min(l3_train)
min_in4 = min(l4_train)
min_in5 = min(l5_train)

for i in range(len(l1_train)):
    l1_train[i] = (((l1_train[i]-min_in1)/(max_in1-min_in1))*(new_max-new_min))+new_min

for i in range(len(l2_train)):
    l2_train[i] = (((l2_train[i]-min_in2)/(max_in2-min_in2))*(new_max-new_min))+new_min

for i in range(len(l3_train)):
    l3_train[i] = (((l3_train[i]-min_in3)/(max_in3-min_in3))*(new_max-new_min))+new_min

for i in range(len(l4_train)):
    l4_train[i] = (((l4_train[i]-min_in4)/(max_in4-min_in4))*(new_max-new_min))+new_min

for i in range(len(l5_train)):
    l5_train[i] = (((l5_train[i]-min_in5)/(max_in5-min_in5))*(new_max-new_min))+new_min

train_dat = []
for i in range(len(l1_train)):
    temp = [l1_train[i], l2_train[i], l3_train[i], l4_train[i], l5_train[i]]
    train_dat.insert(i,temp)

train_dat_arr = np.array(train_dat)


# Initialising parameters

lr1=list(np.random.uniform(0.001,0.1,100)) # generating learning rate which is decreasing from 0.1 to 0.001
lr1.sort(reverse=True)
lr2=lr1
lr3 = lr1
lr4 = lr1
lm = 0.95 #learning momentum is set to 0.95
num_in = 4 #dimension of input
num_out = 1 #dimension of output
num_hn = 20 #number of neurons in each hidden layers

#initialising weights
w1 = np.random.randn(num_in,num_hn)
w2 = np.random.randn(num_hn,num_hn)
w3 = np.random.randn(num_hn,num_hn)
w4 = np.random.randn(num_hn,num_out)

dw00 = np.zeros((num_in,num_hn))
dw01 = np.zeros((num_hn,num_hn))
dw02 = np.zeros((num_hn,num_hn))
dw03 = np.zeros((num_hn,num_out))
ep = 0 #initial epoch
mse = []

# Activation functon
def fun_sig(out, der=False):
    if (der == True):
        return out * (1 - out)
    return 1/(1 + np.exp(-out))


# Performing 500 epochs
while(ep<100):
    #shuffle dataset
    np.random.shuffle(train_dat_arr)
    data_trnsamp = []
    data_trnlab = []
    for i in train_dat_arr:
        data_trnsamp.append([i[:-1]])
        data_trnlab.append([i[-1]])
    data_trnsamp =  np.array(data_trnsamp)
    data_trnlab = np.array(data_trnlab)
    error = []
    #iterating over each data with min-Batch size = 1
    for i in range(len(data_trnsamp)):   

        #Forward Computation

        out1 = np.dot(data_trnsamp[i],w1)
        out1_up = fun_sig(out1)
        out2 = np.dot(out1_up,w2)
        out2_up = fun_sig(out2)
        out3 = np.dot(out2_up,w3)
        out3_up = fun_sig(out3)
        out4 = np.dot(out3_up,w4)
        out4_up = fun_sig(out4)
        
        #error
        er = data_trnlab[i] - out4_up
        error.append(er)

        #BackPropogation

        delta1 = er * fun_sig(out4_up, der=True)
        delta2 = fun_sig(out3_up, der=True) * (np.dot(delta1,w4.T))
        delta3 = fun_sig(out2_up,der=True) * (np.dot(delta2,w3.T))
        delta4 = fun_sig(out1_up,der=True) * (np.dot(delta3,w2.T))


        dw1 = lr1[ep]* np.dot(data_trnsamp[i].T,delta4) 
        dw2 = lr2[ep]* np.dot(out1_up.T,delta3)
        dw3 = lr3[ep]* np.dot(out2_up.T,delta2) 
        dw4 = lr4[ep]* np.dot(out3_up.T,delta1)

        w1_up = w1 + (lm * dw00) +dw1
        w2_up = w2 + (lm * dw01) +dw2
        w3_up = w3 + (lm * dw02) +dw3
        w4_up = w4 + (lm * dw03) +dw4

        w1 = w1_up
        w2 = w2_up
        w3 = w3_up
        w4 = w4_up
        dw00 = dw1
        dw01 = dw2
        dw02 = dw3
        dw03 = dw4            
    ep +=1
    mse.append(np.sum(np.mean(np.square(error))))

#Calculating RMSE of Training
print("Training RMSE : ", np.sqrt(mse[-1]))

# Graph of epoch vs mse

ep = list(range(0,100))   
plt.plot(ep,mse)
plt.xlabel('Epochs')
plt.ylabel('MSE')
plt.title('Learning Curve')
plt.show()


# Testing Data

l1_test = list(test['AT'])
l2_test = list(test['V'])
l3_test = list(test['AP'])
l4_test = list(test['RH'])
l5_test = list(test['PE'])

#Normalising Testing data

new_max1 = 1
new_min1 = 0
max_in1t = max(l1_test)
max_in2t = max(l2_test)
max_in3t = max(l3_test)
max_in4t = max(l4_test)
max_in5t = max(l5_test)
min_in1t = min(l1_test)
min_in2t = min(l2_test)
min_in3t = min(l3_test)
min_in4t = min(l4_test)
min_in5t = min(l5_test)

for i in range(len(l1_test)):
    l1_test[i] = (((l1_test[i]-min_in1t)/(max_in1t-min_in1t))*(new_max1-new_min1))+new_min1

for i in range(len(l2_test)):
    l2_test[i] = (((l2_test[i]-min_in2t)/(max_in2t-min_in2t))*(new_max1-new_min1))+new_min1

for i in range(len(l3_test)):
    l3_test[i] = (((l3_test[i]-min_in3t)/(max_in3t-min_in3t))*(new_max1-new_min1))+new_min1

for i in range(len(l4_test)):
    l4_test[i] = (((l4_test[i]-min_in4t)/(max_in4t-min_in4t))*(new_max1-new_min1))+new_min1

for i in range(len(l5_test)):
    l5_test[i] = (((l5_test[i]-min_in5t)/(max_in5t-min_in5t))*(new_max1-new_min1))+new_min1


test_dat = []
for i in range(len(l1_test)):
    temp1 = [l1_test[i], l2_test[i], l3_test[i], l4_test[i], l5_test[i]]
    test_dat.insert(i,temp1)

test_dat_arr = np.array(test_dat)

print(test_dat_arr[1])

data_testsamp = []
data_testlab = []
for i in test_dat_arr:
    data_testsamp.append(i[:-1])
    data_testlab.append([i[-1]])
data_testsamp =  np.array(data_testsamp)
data_testlab = np.array(data_testlab)


# Performing testing with our updated weights
out_test = []
error_test = []
for i in range(len(data_testsamp)):   

    #Forward Computation
    out1 = np.dot(data_testsamp[i],w1)
    out1_up = fun_sig(out1)
    out2 = np.dot(out1_up,w2)
    out2_up = fun_sig(out2)
    out3 = np.dot(out2_up,w3)
    out3_up = fun_sig(out3)
    out4 = np.dot(out3_up,w4)
    out4_up = fun_sig(out4)
    
    out_test.append(out4_up)
    error_test.append(data_testlab[i]-out4_up)

#Calculating RMSE of testing 

rmse_test = np.sqrt(np.sum(np.mean(np.square(error_test))))

print("Testing RMSE : ", rmse_test)

