#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
import numpy as np

# capturing data from the txt file of dataset
my_file = open("occupancy_data/datatraining.txt")
content = my_file.readlines()
content = [x.strip() for x in content]

l1 = []
for i in content:
    l = i.split(",")
    l1.append(l)

#formulating each input and output in unique list from dataset
leng1 = len(l1)
leng2 = len(l1[1])
temp = []
hum = []
light = []
co2 = []
humratio = []
occ = []

for i in range(1,leng1):
    for j in range(2,leng2):
        if j == 2:
            temp.append(float(l1[i][j]))
        if j==3:
            hum.append(float(l1[i][j]))
        if j==4:
            light.append(float(l1[i][j]))
        if j==5:
            co2.append(float(l1[i][j]))
        if j==6:
            humratio.append(float(l1[i][j]))
        if j==7:
            occ.append(float(l1[i][j]))
            
# normalization od data to range 0 to 1            
l_temp = min(temp)
m_temp = max(temp)

l_hum = min(hum)
m_hum = max(hum)

l_light = min(light)
m_light = max(light)

l_co2 = min(co2)
m_co2 = max(co2)

l_humratio = min(humratio)
m_humratio = max(humratio)

new_max = 1
new_min = 0

for i in range(len(temp)):
        temp[i] = (((temp[i]-l_temp)/(m_temp-l_temp))*(new_max-new_min))+new_min
        hum[i] = (((hum[i]-l_hum)/(m_hum-l_hum))*(new_max-new_min))+new_min
        light[i] = (((light[i]-l_light)/(m_light-l_light))*(new_max-new_min))+new_min
        co2[i] = (((co2[i]-l_co2)/(m_co2-l_co2))*(new_max-new_min))+new_min
        humratio[i] = (((humratio[i]-l_humratio)/(m_humratio-l_humratio))*(new_max-new_min))+new_min

data_trn = []
for i in range(len(temp)):
    u = [temp[i],hum[i],light[i],co2[i],humratio[i],occ[i]]
    data_trn.append(u)

data_trn = np.array(data_trn)


# Initialising parameters

lr1=list(np.random.uniform(0.01,0.1,1000))#initialising learning rate
lr1.sort(reverse=True)
lr2=lr1
lr3 = lr1
lr4 = lr1
lm = 0.95 #learning momentum
num_in = 5 # dimension of input
num_out = 1 #dimension of output
num_hn = 20#number of hidden neurons
w1 = np.random.randn(num_in,num_hn)
w2 = np.random.randn(num_hn,num_hn)
w3 = np.random.randn(num_hn,num_hn)
w4 = np.random.randn(num_hn,num_out)
dw00 = np.zeros((num_in,num_hn))
dw01 = np.zeros((num_hn,num_hn))
dw02 = np.zeros((num_hn,num_hn))
dw03 = np.zeros((num_hn,num_out))
ep = 0 #initial epoch
mse = []

# Activation Function
def fun_sig(out, der=False):
    if (der == True):
        return out * (1 - out)
    return 1/(1 + np.exp(-out))

#Performing training over 1000 epochs

while(ep<1000):
    np.random.shuffle(data_trn)
    data_trnsamp = []
    data_trnlab = []
    for i in data_trn:
        data_trnsamp.append([i[:-1]])
        data_trnlab.append([i[-1]])
    data_trnsamp =  np.array(data_trnsamp)
    data_trnlab = np.array(data_trnlab)
    error = []
    for i in range(len(data_trnsamp)):   

        #Forward Computation

        out1 = np.dot(data_trnsamp[i],w1)
        out1_up = fun_sig(out1)
        out2 = np.dot(out1_up,w2)
        out2_up = fun_sig(out2)
        out3 = np.dot(out2_up,w3)
        out3_up = fun_sig(out3)
        out4 = np.dot(out3_up,w4)
        out4_up = fun_sig(out4)

        er = float(data_trnlab[i] - out4_up)
        error.append(er)

        #BackPropogation

        delta1 = er * fun_sig(out4_up, der=True)
        delta2 = fun_sig(out3_up, der=True) * (np.dot(delta1,w4.T))
        delta3 = fun_sig(out2_up,der=True) * (np.dot(delta2,w3.T))
        delta4 = fun_sig(out1_up,der=True) * (np.dot(delta3,w2.T))


        dw1 = lr1[ep]* np.dot(data_trnsamp[i].T,delta4) 
        dw2 = lr2[ep]* np.dot(out1_up.T,delta3)
        dw3 = lr3[ep]* np.dot(out2_up.T,delta2) 
        dw4 = lr4[ep]* np.dot(out3_up.T,delta1)

        w1_up = w1 + (lm * dw00) +dw1
        w2_up = w2 + (lm * dw01) +dw2
        w3_up = w3 + (lm * dw02) +dw3
        w4_up = w4 + (lm * dw03) +dw4

        w1 = w1_up
        w2 = w2_up
        w3 = w3_up
        w4 = w4_up
        dw00 = dw1
        dw01 = dw2
        dw02 = dw3
        dw03 = dw4            
    ep +=1
    mse.append(np.sum(np.mean(np.square(error))))

#plotting graph between epochs and mse

ep = list(range(0,1000))   
plt.plot(ep,mse)
plt.xlabel('Epochs')
plt.ylabel('MSE')
plt.title('Learning Curve')
plt.show()

# Capturing Testing data
# capturing data from the txt file of dataset
my_file1 = open("occupancy_data/datatest.txt")
content1 = my_file1.readlines()
content1 = [x.strip() for x in content1]

l1te = []

for i in content1:
    lte = i.split(",")
    l1te.append(lte)

#formulating each input and output in unique list from dataset
leng1te = len(l1te)
leng2te = len(l1te[1])
tempte = []
humte = []
lightte = []
co2te = []
humratiote = []
occte = []

for i in range(1,leng1te):
    for j in range(2,leng2te):
        if j == 2:
            tempte.append(float(l1te[i][j]))
        if j==3:
            humte.append(float(l1te[i][j]))
        if j==4:
            lightte.append(float(l1te[i][j]))
        if j==5:
            co2te.append(float(l1te[i][j]))
        if j==6:
            humratiote.append(float(l1te[i][j]))
        if j==7:
            occte.append(float(l1te[i][j]))
            
# normalization od data to range 0 to 1            
l_tempte = min(tempte)
m_tempte = max(tempte)

l_humte = min(humte)
m_humte = max(humte)

l_lightte = min(lightte)
m_lightte = max(lightte)

l_co2te = min(co2te)
m_co2te = max(co2te)

l_humratiote = min(humratiote)
m_humratiote = max(humratiote)

new_maxte = 1
new_minte = 0

for i in range(len(tempte)):
        tempte[i] = (((tempte[i]-l_tempte)/(m_tempte-l_tempte))*(new_maxte-new_minte))+new_minte
        humte[i] = (((humte[i]-l_humte)/(m_humte-l_humte))*(new_maxte-new_minte))+new_minte
        lightte[i] = (((lightte[i]-l_lightte)/(m_lightte-l_lightte))*(new_maxte-new_minte))+new_minte
        co2te[i] = (((co2te[i]-l_co2te)/(m_co2te-l_co2te))*(new_maxte-new_minte))+new_minte
        humratiote[i] = (((humratiote[i]-l_humratiote)/(m_humratiote-l_humratiote))*(new_maxte-new_minte))+new_minte

data_test = []
for i in range(len(tempte)):
    u = [tempte[i],humte[i],lightte[i],co2te[i],humratiote[i],occte[i]]
    data_test.append(u)

data_test = np.array(data_test)
data_testsamp = []
data_testlab = []
for i in data_test:
    data_testsamp.append([i[:-1]])
    data_testlab.append([i[-1]])
data_testsamp =  np.array(data_testsamp)
data_testlab = np.array(data_testlab)


# Performing testing based on  our updated parameters

out_test = []
for i in range(len(data_testsamp)):   

    #Forward Computation
    out1 = np.dot(data_testsamp[i],w1)
    out1_up = fun_sig(out1)
    out2 = np.dot(out1_up,w2)
    out2_up = fun_sig(out2)
    out3 = np.dot(out2_up,w3)
    out3_up = fun_sig(out3)
    out4 = np.dot(out3_up,w4)
    out4_up = fun_sig(out4)
    
    out_test.append(out4_up)

# Calculating accuracy
misclassified_samp = 0
for i in range(len(out_test)):
    er = data_testlab[i]-out_test[i][0]
    if er > 0.01:
        misclassified_samp += 1
acc = 1-(misclassified_samp/len(out_test))
print("Testing Accuracy : ", acc)

